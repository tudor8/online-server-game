# Online Server Game

Simple server that hosts a local website-game.

# Getting Started

* This was made for one of my universities module.

* Done with many similarities to the Spring framework.

# Running

Read the attached report for more information.
 
# Built With

* [Java]

# Authors

* **Tudor Gheorghe** - (http://www.tudorgheorghe.net)
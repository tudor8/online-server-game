package main.controllers;

import main.services.CardService;

import javax.inject.Singleton;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

@Path("/cards")
@Produces(MediaType.APPLICATION_JSON)
@Singleton
public class CardController {
    private CardService cardService;

    public CardController () {
        cardService = CardService.getInstance();
    }

    @POST
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Path("/patchCard")
    public void patchCard(@FormParam("status") String status, @FormParam("playerId") int playerId, @FormParam("cardId") int cardId) {
        cardService.patchCard(status, playerId, cardId);
    }
}

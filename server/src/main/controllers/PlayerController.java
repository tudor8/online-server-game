package main.controllers;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import main.services.PlayerService;

import javax.inject.Singleton;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

@Path("/players")
@Produces(MediaType.APPLICATION_JSON)
@Singleton
public class PlayerController {
    public static final Gson GSON = new GsonBuilder()
            .setPrettyPrinting()
            .disableHtmlEscaping()
            .create();

    private PlayerService playerService;

    public PlayerController () {
        playerService = PlayerService.getInstance();
    }

    @GET
    @Path("/getAll")
    public String getPlayers() {
        return GSON.toJson(playerService.getPlayers());
    }

    @POST
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Path("/newPlayer")
    public String addNewPlayer(@FormParam("name") String name) {
        return GSON.toJson(playerService.addNewPlayer(name));
    }

    @POST
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Path("/patchPlayer")
    public void patchPlayer(@FormParam("status") String status, @FormParam("playerId") int playerId) {
        playerService.patchPlayer(status, playerId);
    }
}

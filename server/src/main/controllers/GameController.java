package main.controllers;

import main.entities.GameSettings;
import main.entities.GameState;
import main.enums.CardType;
import main.services.GameService;

import javax.inject.Singleton;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

@Path("/game")
@Produces(MediaType.APPLICATION_JSON)
@Singleton
public class GameController {
    private GameService gameService;

    public GameController() {
        gameService = GameService.getInstance();
    }

    @GET
    @Path("/settings")
    public GameSettings getGameSettings() {
        return gameService.getGameSettings();
    }

    @GET
    @Path("/state")
    public GameState getGameState() {
        return gameService.getGameState();
    }

    @GET
    @Path("/isGameFull")
    public boolean isGameFull() {
        return gameService.isGameFull();
    }

    @GET
    @Path("/isMoveAllowed")
    public boolean isMoveAllowed(
            @QueryParam("x") final int x,
            @QueryParam("y") final int y,
            @QueryParam("playerId")final int playerId,
            @QueryParam("cardType")final String cardType) {
        CardType type;
        try {
            type = CardType.valueOf(cardType.toUpperCase());
        }
        catch (IllegalArgumentException e) {
            type = CardType.NONE;
        }

        return gameService.isMoveAllowed(x, y, playerId, type);
    }

    @POST
    @Path("/makeMove")
    public void makeMove(final @FormParam("x") int x, final @FormParam("y") int y, final @FormParam("playerId") int playerId) {
        gameService.makeMove(x, y, playerId);
    }

    @POST
    @Path("/nextPlayer")
    public void nextPlayer() {
        gameService.nextPlayer();
    }
}

package main.enums;

import com.google.gson.annotations.SerializedName;

public enum CardStatus {
    @SerializedName("Used")
    USED,

    @SerializedName("Available")
    AVAILABLE,

    @SerializedName("Selected")
    SELECTED
}

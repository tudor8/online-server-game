package main.enums;

import com.google.gson.annotations.SerializedName;

public enum GameStatus {
    WAITING,

    STARTED,

    OVER
}

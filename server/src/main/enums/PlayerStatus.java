package main.enums;

import com.google.gson.annotations.SerializedName;

public enum PlayerStatus {
    @SerializedName("Not Ready")
    NOTREADY,

    @SerializedName("Ready")
    READY,

    @SerializedName("Playing")
    PLAYING,

    @SerializedName("Stuck")
    LOST
}

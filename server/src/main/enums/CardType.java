package main.enums;

import com.google.gson.annotations.SerializedName;

public enum CardType {
    @SerializedName("Place 2")
    MULTIPLACE,

    @SerializedName("Replace")
    REPLACE,

    @SerializedName("Place Anywhere")
    ANYWHERE,

    @SerializedName("None")
    NONE;
}

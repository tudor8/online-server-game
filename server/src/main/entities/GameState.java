package main.entities;

import com.google.gson.annotations.Expose;
import main.enums.GameStatus;

import java.util.Arrays;


public class GameState {
    private int[][] colours;
    private int currentPlayer;
    private GameStatus status;

    public int[][] getColours() {
        return colours;
    }

    public void setColours(int[][] colours) {
        this.colours = colours;
    }

    public int getCurrentPlayer() {
        return currentPlayer;
    }

    public void setCurrentPlayer(int currentPlayer) {
        this.currentPlayer = currentPlayer;
    }

    public GameStatus getStatus() {
        return status;
    }

    public void setStatus(GameStatus status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "GameState{" +
                "colours=" + Arrays.toString(colours[0]) +
                ", currentPlayer=" + currentPlayer +
                ", status=" + status +
                '}';
    }
}

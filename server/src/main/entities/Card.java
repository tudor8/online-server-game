package main.entities;

import main.enums.CardStatus;
import main.enums.CardType;

import java.util.Objects;

public class Card {
    private CardType type;
    private CardStatus status;

    public CardType getType() {
        return type;
    }

    public void setType(CardType type) {
        this.type = type;
    }

    public CardStatus getState() {
        return status;
    }

    public void setState(CardStatus state) {
        this.status = state;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Card card = (Card) o;
        return type == card.type &&
                status == card.status;
    }

    @Override
    public int hashCode() {
        return Objects.hash(type, status);
    }
}

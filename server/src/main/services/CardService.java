package main.services;

import main.entities.Card;
import main.entities.Player;
import main.enums.CardStatus;
import main.enums.CardType;

import javax.inject.Singleton;

@Singleton
public class CardService {
    private static CardService instance;

    private PlayerService playerService;

    public static CardService getInstance() {
        if(instance == null) {
            instance = new CardService();
        }
        return instance;
    }

    private CardService() {
        instance = this;
        playerService = PlayerService.getInstance();
    }

    /**
     * Create a set based on the CardType enum which will contain AVAILABLE cards of each type.
     */
    public Card[] getDefaultCards() {
        Card[] cards = new Card[CardType.values().length - 1];
        for(int i = 0; i < CardType.values().length - 1; i++) {
            cards[i] = new Card();
            cards[i].setType(CardType.values()[i]);
            cards[i].setState(CardStatus.AVAILABLE);
        }

        return cards;
    }

    /**
     * Should be used to update specific fields of a specific card when called by the frontend.
     * @return whether the update was successful
     */
    public boolean patchCard(final String state, final int playerId, final int cardId) {
        final Player player = playerService.getPlayer(playerId);
        final Card card = player.getCards()[cardId];

        try {
            final CardStatus stateEnum = CardStatus.valueOf(state.toUpperCase());
            if(stateEnum == CardStatus.SELECTED) {
                player.setSelectedCard(cardId);
            }
            else if(card.getState() == CardStatus.SELECTED) {
                player.setSelectedCard(-1);
            }
            card.setState(stateEnum);
        }
        catch (IllegalArgumentException e) {
            return false;
        }

        return true;
    }

    public Card getCardByType(final CardType type, final int playerId) {
        Player player = playerService.getPlayer(playerId);
        for(Card card : player.getCards()) {
            if(card.getType() == type) {
                return card;
            }
        }
        return null;
    }
}

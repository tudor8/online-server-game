package main.services;

import main.entities.Player;
import main.enums.PlayerStatus;

import javax.inject.Singleton;
import java.util.*;

@Singleton
public class PlayerService {
    private static PlayerService instance;

    private static int playerIdCount = 0;

    private CardService cardService;

    private GameService gameService;

    private Map<Integer, Player> players;

    public static PlayerService getInstance() {
        if(instance == null) {
            instance = new PlayerService();
        }
        return instance;
    }

    public void reset() {
        players = new HashMap<>();
        playerIdCount = 0;
    }

    private PlayerService() {
        instance = this;
        cardService = CardService.getInstance();
        gameService = GameService.getInstance();
        players = new HashMap<>();
        playerIdCount = 0;
    }

    public synchronized Player addNewPlayer(final String name) {
        playerIdCount++;

        Player player = new Player();
        player.setId(playerIdCount);
        player.setName(name);
        player.setColor(players.size() + 1);
        player.setCards(cardService.getDefaultCards());
        player.setStatus(PlayerStatus.NOTREADY);
        player.setPoints(1);
        player.setSelectedCard(-1);

        players.put(playerIdCount, player);

        return player;
    }

    public Collection<Player> getPlayers() {
        return players.values();
    }

    public Player getPlayer(final int playerId) {
        return players.get(playerId);
    }

    /**
     * Should be used to update specific fields of a specific player when called by the frontend.
     * @return whether the update was successful
     */
    public boolean patchPlayer(final String status, final int playerId) {
        final Player player = getPlayer(playerId);

        try {
            final PlayerStatus statusEnum = PlayerStatus.valueOf(status.toUpperCase());
            player.setStatus(statusEnum);
        }
        catch (IllegalArgumentException e) {
            return false;
        }

        gameService.checkIfGameShouldStart();

        return true;
    }
}

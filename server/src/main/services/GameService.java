package main.services;

import main.entities.Card;
import main.entities.GameSettings;
import main.entities.GameState;
import main.entities.Player;
import main.enums.CardStatus;
import main.enums.CardType;
import main.enums.GameStatus;
import main.enums.PlayerStatus;
import main.utility.Vector2;

import javax.inject.Singleton;
import java.awt.*;
import java.util.*;
import java.util.List;

@Singleton
public class GameService {
    public final static Vector2 GAME_SIZE = new Vector2(6, 10);

    public final static int MAXIMUM_PLAYERS = 6;

    public final static List<String> AVAILABLE_COLOURS = Arrays.asList(
            toHex(Color.white),
            toHex(Color.red),
            toHex(Color.green),
            toHex(Color.blue),
            toHex(Color.cyan),
            toHex(Color.orange),
            toHex(Color.yellow),
            toHex(Color.pink),
            toHex(Color.magenta)
    );

    public final static List<Vector2> NEIGHBOUR_COORDS = Arrays.asList(
            new Vector2(-1, -1),
            new Vector2(-1,  0),
            new Vector2(-1,  1),
            new Vector2( 0, -1),
            new Vector2( 0, +1),
            new Vector2( 1, -1),
            new Vector2( 1,  0),
            new Vector2( 1, +1)
    );

    private static GameService instance;

    private PlayerService playerService;

    private CardService cardService;

    private GameState gameState;

    public static GameService getInstance() {
        if(instance == null) {
            instance = new GameService();
        }
        return instance;
    }

    public void reset() {
        gameState = new GameState();
        gameState.setColours(new int[GAME_SIZE.getX()][GAME_SIZE.getY()]);
        gameState.setStatus(GameStatus.WAITING);
        gameState.setCurrentPlayer(1);
    }

    private GameService () {
        instance = this;
        playerService = PlayerService.getInstance();
        cardService = CardService.getInstance();
        gameState = new GameState();
        gameState.setColours(new int[GAME_SIZE.getX()][GAME_SIZE.getY()]);
        gameState.setStatus(GameStatus.WAITING);
        gameState.setCurrentPlayer(1);
    }

    /**
     * Convert a color to its hex string.
     */
    private static String toHex(final Color color) {
        String hex = Integer.toHexString(color.getRGB());
        return hex.substring(2);
    }

    public GameSettings getGameSettings() {
        GameSettings gameSettings = new GameSettings();
        gameSettings.setColours(AVAILABLE_COLOURS.toArray(new String[AVAILABLE_COLOURS.size()]));
        gameSettings.setMaximumPlayers(MAXIMUM_PLAYERS);
        gameSettings.setX(GAME_SIZE.getX());
        gameSettings.setY(GAME_SIZE.getY());

        return gameSettings;
    }

    public GameState getGameState() {
        return gameState;
    }

    /**
     * Checks whether a given move would be allowed under game settings.
     *  A normal move can only be done to free nearby cells.
     *  A replace move can only be done to occupied nearby cells.
     *  A anywhere move can only be done to free anywhere cells.
     *  A place twice move counts as a normal move.
     * @param cardType : the type of the move
     * @return whether the move is allowed or not (note the move isn't executed yet)
     */
    public boolean isMoveAllowed(final int x, final int y, final int playerId, final CardType cardType) {
        boolean allowed = false;

        switch (cardType) {
            case REPLACE:
                if(getCell(x, y) != playerId && getCell(x, y) != 0) {
                    Vector2 coords = new Vector2(x, y);
                    allowed = isThereAPlayerCellAroundCell(coords, playerId);
                }
                break;

            case ANYWHERE:
                if(getCell(x, y) == 0) {
                    allowed = true;
                }
                break;

            default:
                if(getCell(x, y) != 0) {
                    break;
                }

                Vector2 coords = new Vector2(x, y);
                allowed = isThereAPlayerCellAroundCell(coords, playerId);
        }

        return allowed;
    }

    /**
     * Checks whether there is a player around a given cell. Checks in all directions.
     */
    private boolean isThereAPlayerCellAroundCell(final Vector2 coords, final int playerId) {
        for(Vector2 neighbour : NEIGHBOUR_COORDS) {
            Vector2 position = coords.add(neighbour);
            if(position.isPositionValid(GAME_SIZE)) {
                if(getCell(position.getX(), position.getY()) == playerId) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Executes a given move. This doesn't have checking and should only be executed after calling <isMoveAllowed>
     * Will add and subtract points from players.
     * Will check if any players lost and if the game ended.
     */
    public void makeMove(final int x, final int y, final int playerId) {
        Player player = playerService.getPlayer(playerId);
        if( getCell(x, y) != 0) {
            Player otherPlayer = playerService.getPlayer(getCell(x, y));
            otherPlayer.setPoints(otherPlayer.getPoints() - 1);
        }
        gameState.getColours()[x][y] = playerId;
        player.setPoints(player.getPoints() + 1);
        checkIfPlayersLost();
        checkIfGameIsOver();
    }

    /**
     * Should be called to indicate that the server should select the next player.
     * Players that lost are skipped.
     */
    public void nextPlayer() {
        Collection<Player> players = playerService.getPlayers();
        List<Player> playerList = new ArrayList<>(players);
        int id = gameState.getCurrentPlayer();
        int nextId = id + 1;
        if(nextId > playerService.getPlayers().size()) { nextId = 1; }

        while(id != nextId) {
            if(playerList.get(nextId - 1).getStatus() != PlayerStatus.LOST) {
                break;
            }

            nextId = nextId + 1;
            if(nextId > playerService.getPlayers().size()) { nextId = 1; }
        }


        gameState.setCurrentPlayer(nextId);
    }

    /**
     * Checks if the game should be started.
     * This happens if there are at least 2 players and all players are READY.
     * If the conditions are met, the server picks random starting locations for the players.
     *  The locations chosen cannot be on the edge of the field.
     */
    public synchronized void checkIfGameShouldStart() {
        Collection<Player> players = playerService.getPlayers();
        boolean shouldStart = true;

        if(players.size() < 2) {
            shouldStart = false;
        }
        else {
            for (Player player : players) {
                if (player.getStatus() != PlayerStatus.READY) {
                    shouldStart = false;
                }
            }
        }

        if(shouldStart) {
            gameState.setStatus(GameStatus.STARTED);
            List<Vector2> possiblePositions = new ArrayList<>();
            for (int row = 1; row < gameState.getColours().length - 2; row++) {
                for (int col = 1; col < gameState.getColours()[0].length - 2; col++) {
                    possiblePositions.add(new Vector2(row, col));
                }
            }

            Collections.shuffle(possiblePositions);

            int i = 0;
            for(Player player : players) {
                gameState.getColours()[possiblePositions.get(i).getX()][possiblePositions.get(i).getY()] = player.getId();
                i++;
                playerService.patchPlayer(PlayerStatus.PLAYING.toString(), player.getId());
            }
        }
    }

    /**
     * Checks if the game has ended. This is true if there are no empty cells anymore.
     */
    public void checkIfGameIsOver() {
        boolean isOver = true;
        for (int row = 0; row < gameState.getColours().length; row++) {
            for (int col = 0; col < gameState.getColours()[0].length; col++) {
               if(gameState.getColours()[row][col] == 0) {
                   isOver = false;
               }
            }
        }
        if(isOver) {
            gameState.setStatus(GameStatus.OVER);
        }
    }

    public boolean isGameFull() {
        Collection<Player> players = playerService.getPlayers();
        return players.size() >= MAXIMUM_PLAYERS ||
                gameState.getStatus() == GameStatus.STARTED ||
                gameState.getStatus() == GameStatus.OVER;
    }

    /**
     * Checks if any players lost.
     * This is done by going through every cell and if they are empty, checking for any nearby players.
     * In other words, If any player has a nearby open cell, they haven't lost yet.
     * Note: even if a player has no free cells nearby, they haven't lost yet if they still have their replace or anywhere card.
     */
    public void checkIfPlayersLost() {
        Map<Integer, Boolean> hasPlayerLost = new HashMap<>();
        int numberOfEmptySpots = 0;

        for(Player player : playerService.getPlayers()) {
            boolean initialState = true;
            if(player.getStatus() == PlayerStatus.LOST) {
                initialState = false;
            }
            hasPlayerLost.put(player.getId(), initialState);
        }

        for (int row = 0; row < gameState.getColours().length; row++) {
            for (int col = 0; col < gameState.getColours()[0].length; col++) {
                // If a player has not lost yet, check if there are no neighbours available to mark them as lost
                if(getCell(row, col) != 0 && hasPlayerLost.get(getCell(row, col))) {
                    Vector2 coords = new Vector2(row, col);
                    for(Vector2 neighbour : NEIGHBOUR_COORDS) {
                        Vector2 position = coords.add(neighbour);
                        if(position.isPositionValid(GAME_SIZE)) {
                            if(getCell(position.getX(), position.getY()) == 0) {
                                hasPlayerLost.put(getCell(row, col), false);
                            }
                        }
                    }
                }
                else if(getCell(row, col) == 0) {
                    numberOfEmptySpots++;
                }
            }
        }

        for(Map.Entry<Integer, Boolean> entry : hasPlayerLost.entrySet()) {
            Card anywhereCard = cardService.getCardByType(CardType.ANYWHERE, entry.getKey());
            Card replaceCard  = cardService.getCardByType(CardType.REPLACE , entry.getKey());

            if(anywhereCard != null && anywhereCard.getState() != CardStatus.USED && numberOfEmptySpots > 0) {
                continue;
            }

            if(replaceCard != null && replaceCard.getState() != CardStatus.USED) {
                continue;
            }

            if(entry.getValue()) {
                playerService.patchPlayer(PlayerStatus.LOST.toString(), entry.getKey());
            }
        }
    }



    private int getCell(int row, int col) {
        return gameState.getColours()[row][col];
    }
}

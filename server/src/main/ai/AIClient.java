package main.ai;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import main.entities.Card;
import main.entities.GameSettings;
import main.entities.GameState;
import main.entities.Player;
import main.enums.CardStatus;
import main.enums.CardType;
import main.enums.GameStatus;
import main.services.GameService;
import main.utility.Vector2;
import main.webserver.GsonMessageBodyHandler;
import main.webserver.TomcatServer;
import org.glassfish.jersey.client.ClientConfig;

import javax.ws.rs.client.*;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedHashMap;
import javax.ws.rs.core.MultivaluedMap;
import java.lang.reflect.Type;
import java.util.*;

public class AIClient {
    public final static int DEFAULT_NUMBER_OF_BOTS = 2;

    public final static List<String> NAMES = Arrays.asList("James", "Tomo", "Not a Bot", "Awesome Guy", "Mister mister", "Rando");

    public final WebTarget target;

    public static final Gson GSON = new Gson();

    private Player player;

    private GameSettings gameSettings;

    private Vector2[] cornerCoordinates;

    private String name;

    public AIClient(String name) throws InterruptedException {
        ClientConfig config = new ClientConfig(GsonMessageBodyHandler.class);
        target = ClientBuilder.newClient(config).target(TomcatServer.INDEX_URL);

        this.name = name;
        new Thread(() -> {
            try {
                loop();
            }
            catch (InterruptedException e) {
                e.printStackTrace();
            }
        }).start();

    }

    private void loop() throws InterruptedException {
        player = newPlayer();
        becomeReady();
        gameSettings = getGameSettings();
        calculateCorners(gameSettings);

        while (true) {
            GameState state = getGameState();
            List<Player> players = getAllPlayers();
            for (Player oPlayer : players) {
                if (player.getId() == oPlayer.getId()) {
                    player = oPlayer;
                }
            }

            if (state.getStatus() == GameStatus.STARTED && state.getCurrentPlayer() == player.getId()) {
                executeLogic(state, players);
            }

            Thread.sleep(100);
        }
    }

    private void calculateCorners(GameSettings gameSettings) {
        cornerCoordinates = new Vector2[4];
        cornerCoordinates[0] = new Vector2(0, 0);
        cornerCoordinates[1] = new Vector2(gameSettings.getX() - 1, 0);
        cornerCoordinates[2] = new Vector2(0, gameSettings.getY());
        cornerCoordinates[3] = new Vector2(gameSettings.getX(), gameSettings.getY());
    }

    /**
     * Calculate the corner that is farthest away from any other player.
     */
    private Vector2 calculateBestCorner(List<Vector2> otherPlayerBounds, Vector2 selfBounds) {
        Vector2 closest = new Vector2(0, 0);
        double distance = 0;
        for(Vector2 corner : cornerCoordinates) {
            double totalDistance = 0;
            for(Vector2 bounds : otherPlayerBounds) {
                totalDistance += corner.distance(bounds);
            }

            totalDistance /= otherPlayerBounds.size();

            if(totalDistance > distance) {
                distance = totalDistance;
                closest = corner;
            }
        }

        return closest;
    }

    private Vector2 calculateSphereOfInfluence(List<Vector2> possibleMoves) {
        Vector2 total = new Vector2(0, 0);
        for(Vector2 move : possibleMoves) {
            total = total.add(move);
        }
        total = new Vector2(total.getX() / possibleMoves.size(), total.getY() / possibleMoves.size());

        return total;
    }

    private void executeLogic(GameState state, List<Player> players) throws InterruptedException {
        Thread.sleep(1000);

        List<Vector2> emptySpots   = new ArrayList<>(); // A list with all empty spots
        List<Vector2> closeEnemies = new ArrayList<>(); // A list with all nearby enemy tiles
        Map<Integer, List<Vector2>> possibleMoves = new HashMap<>(); // Maps each player to their possible moves

        for (Player player : players) {
            possibleMoves.put(player.getId(), new ArrayList<>());
        }

        // Gather up information about the board
        for (int row = 0; row < state.getColours().length; row++) {
            for (int col = 0; col < state.getColours()[0].length; col++) {
                int cell = state.getColours()[row][col];
                if (cell == 0) {
                    emptySpots.add(new Vector2(row, col));
                    continue;
                }

                for (Vector2 neighbour : GameService.NEIGHBOUR_COORDS) {
                    Vector2 position = new Vector2(row, col).add(neighbour);
                    if (position.isPositionValid(GameService.GAME_SIZE)) {
                        int neighbourCell = state.getColours()[position.getX()][position.getY()];
                        if (neighbourCell == 0) {
                            possibleMoves.get(cell).add(position);
                        }
                        if (cell == player.getId() && neighbourCell != player.getId() && neighbourCell != 0) {
                            closeEnemies.add(position);
                        }
                    }
                }
            }
        }

        // All Anywhere and Replace cards
        Card anywhereCard = getCardByType(CardType.ANYWHERE);
        Card replaceCard  = getCardByType(CardType.REPLACE);
        Card doubleCard  = getCardByType(CardType.MULTIPLACE);
        Map<Vector2, CardType> anywhereMoves = new HashMap<>();
        Map<Vector2, CardType> replaceMoves  = new HashMap<>();
        if (replaceCard != null && replaceCard.getState() != CardStatus.USED) {
            for (Vector2 move : closeEnemies) {
                replaceMoves.put(move, CardType.REPLACE);
            }
        }
        if (anywhereCard != null && anywhereCard.getState() != CardStatus.USED) {
            for (Vector2 move : emptySpots) {
                anywhereMoves.put(move, CardType.ANYWHERE);
            }
        }
        Map<Vector2, CardType> allMoves = new HashMap<>();
        allMoves.putAll(anywhereMoves);
        allMoves.putAll(replaceMoves);

        // Out of moves, so choose either replace or anywhere
        if(possibleMoves.get(player.getId()).size() == 0) {
            if(anywhereMoves.size() == 0 && replaceMoves.size() == 0) {
                nextPlayer();
            }
            else {
                Map.Entry<Vector2, CardType> move = allMoves.entrySet().iterator().next();
                onDecisionMade(move.getKey(), null, move.getValue());
            }
        }
        else if(possibleMoves.get(player.getId()).size() < 5 && allMoves.size() > 0) {
            Map.Entry<Vector2, CardType> move = allMoves.entrySet().iterator().next();
            onDecisionMade(move.getKey(), null, move.getValue());
        }
        else {
            // Calculate each players influence and the closest corner
            List<Vector2> otherPlayerBounds = new ArrayList<>();
            for(Map.Entry<Integer, List<Vector2>> entry : possibleMoves.entrySet()) {
                if(entry.getKey() != player.getId() && entry.getValue().size() > 0) {
                    otherPlayerBounds.add(calculateSphereOfInfluence(entry.getValue()));
                }
            }
            Vector2 personalBounds = calculateSphereOfInfluence(possibleMoves.get(player.getId()));
            Vector2 closestCorner = calculateBestCorner(otherPlayerBounds, personalBounds);

            // Decide on what move to make
            Vector2 closestMove = new Vector2(0, 0);
            double distance = Double.MAX_VALUE;
            for (Vector2 move : possibleMoves.get(player.getId())) {
                double newDistance = move.distance(closestCorner);
                if (newDistance < distance) {
                    distance = newDistance;
                    closestMove = move;
                }
            }

            if(distance < 5 && doubleCard != null && doubleCard.getState() != CardStatus.USED) {
                Vector2 otherMove = new Vector2(0, 0);
                distance = Double.MAX_VALUE;
                for (Vector2 move : possibleMoves.get(player.getId())) {
                    double newDistance = move.distance(closestCorner);
                    if (newDistance < distance && !closestMove.equals(move)) {
                        distance = newDistance;
                        otherMove = move;
                    }
                }

                onDecisionMade(closestMove, otherMove, CardType.MULTIPLACE);
            }
            else {
                onDecisionMade(closestMove, null, CardType.NONE);
            }
        }
    }

    private void onDecisionMade(Vector2 move1, Vector2 move2, CardType type) throws InterruptedException {
        if (type != CardType.NONE) {
            int id = type.ordinal();
            patchCard(CardStatus.SELECTED.toString(), id);

            Thread.sleep(1000);

            patchCard(CardStatus.USED.toString(), id);
        }

        makeMove(move1.getX(), move1.getY());

        if(move2 != null) {
            Thread.sleep(1000);
            makeMove(move2.getX(), move2.getY());
        }

        nextPlayer();
    }

    private Card getCardByType(CardType type) {
        for (Card card : player.getCards()) {
            if (card.getType() == type) {
                return card;
            }
        }
        return null;
    }

    private void nextPlayer() {
        MultivaluedMap<String, String> formData = new MultivaluedHashMap<>();
        executePostRequest("game/nextPlayer", formData);
    }

    private void patchCard(String status, int cardId) {
        MultivaluedMap<String, String> formData = new MultivaluedHashMap<>();
        formData.add("status", status);
        formData.add("playerId", Integer.toString(player.getId()));
        formData.add("cardId", Integer.toString(cardId));
        executePostRequest("cards/patchCard", formData);
    }

    private void makeMove(int row, int col) {
        MultivaluedMap<String, String> formData = new MultivaluedHashMap<>();
        formData.add("x", Integer.toString(row));
        formData.add("y", Integer.toString(col));
        formData.add("playerId", Integer.toString(player.getId()));
        executePostRequest("game/makeMove", formData);
    }

    private GameState getGameState() {
        String response = executeGetRequest("game/state");
        return GSON.fromJson(response, GameState.class);
    }

    private GameSettings getGameSettings() {
        String response = executeGetRequest("game/settings");
        return GSON.fromJson(response, GameSettings.class);
    }

    private boolean isGameFull() {
        String response = executeGetRequest("game/isGameFull");
        return GSON.fromJson(response, Boolean.class);
    }

    private Player newPlayer() {
        MultivaluedMap<String, String> formData = new MultivaluedHashMap<>();
        formData.add("name", name);
        return GSON.fromJson(executePostRequest("players/newPlayer", formData), Player.class);
    }

    private List<Player> getAllPlayers() {
        String response = executeGetRequest("players/getAll");
        Type t = new TypeToken<List<Player>>() {
        }.getType();
        return GSON.fromJson(response, t);
    }

    private void becomeReady() throws InterruptedException {
        Thread.sleep(10000);
        MultivaluedMap<String, String> formData = new MultivaluedHashMap<>();
        formData.add("status", "ready");
        formData.add("playerId", Integer.toString(player.getId()));
        executePostRequest("players/patchPlayer", formData);
    }

    private String executeGetRequest(String path) {
        return target.path(path).request().get(new GenericType<String>() {
        });
    }

    private String executePostRequest(String path, MultivaluedMap<String, String> formData) {
        return target.path(path)
                .request(MediaType.APPLICATION_JSON_TYPE)
                .post(Entity.entity(formData, MediaType.APPLICATION_FORM_URLENCODED_TYPE), String.class);
    }

    public static void main(String[] args) throws InterruptedException {
        int numberOfBots = DEFAULT_NUMBER_OF_BOTS;
        if(args.length != 0) {
            numberOfBots = Integer.parseInt(args[0]);
            if(numberOfBots < 0) {
                numberOfBots = DEFAULT_NUMBER_OF_BOTS;
            }
            else if(numberOfBots > GameService.MAXIMUM_PLAYERS) {
                numberOfBots = GameService.MAXIMUM_PLAYERS;
            }
        }
        List<String> names = new ArrayList<>(NAMES);
        Collections.shuffle(names);
        AIClient[] clients = new AIClient[numberOfBots];
        for(int i = 0; i < numberOfBots; i++) {
            clients[i] =  new AIClient(names.get(i));
        }
        while(true) {

        }
    }
}

package main.webserver;

import java.io.File;

import javax.servlet.ServletException;

import org.apache.catalina.Context;
import org.apache.catalina.LifecycleException;
import org.apache.catalina.startup.Tomcat;
import org.glassfish.jersey.logging.LoggingFeature;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.servlet.ServletContainer;

/**
 * class taken from the CE303 labs
 */
public class TomcatServer {

	public static final int TOMCAT_PORT = 8080;
	public static final String INDEX_URL = "http://localhost:8080/assignment/rest"; // Join this to start the game
	public static final String GAME_URL = "http://localhost:8080/assignment/game.html"; // Join this to start the game

	public TomcatServer() {
	}

	public void start() throws ServletException, LifecycleException {
		// JAX-RS (Jersey) configuration
		ResourceConfig config = new ResourceConfig();
		// Packages where Jersey looks for web service classes
		// Do not forget to add main.webserver package with Gson provider
		config.packages("solutions", "main/webserver", "main/controllers");
		// Enable logging for debugging purposes
		// Comment out next line to turn off logging
		config.property(LoggingFeature.LOGGING_FEATURE_LOGGER_LEVEL, "INFO");
		// Tomcat configuration
		Tomcat tomcat = new Tomcat();
		tomcat.setPort(TOMCAT_PORT);
		// Add web application
		Context context = tomcat.addWebapp("/assignment", new File("./WebContent").getAbsolutePath());
		// Declare Jersey as a servlet
		Tomcat.addServlet(context, "jersey", new ServletContainer(config));
		// Map certain URLs to Jersey
		context.addServletMappingDecoded("/rest/*", "jersey");
		// Start server
		tomcat.start();
		tomcat.getServer().await();
	}

	public static void main(String[] args) throws ServletException, LifecycleException {
		new TomcatServer().start();
	}
}

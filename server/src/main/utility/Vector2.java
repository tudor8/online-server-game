package main.utility;

import java.util.Objects;

/**
 * Utility class for holding two coordinates as a single class.
 */
public class Vector2 {
    private int x;
    private int y;

    public Vector2(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public Vector2 add(Vector2 other) {
        Vector2 result = new Vector2(x + other.x, y + other.y);
        return result;
    }

    public boolean isPositionValid(Vector2 max) {
        return (getX() >= 0 && getX() < max.getX() &&
                getY() >= 0 && getY() < max.getY());
    }

    public double distance(Vector2 other) {
        return Math.sqrt(Math.pow(x, other.x) + Math.pow(y, other.y));
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Vector2 vector2 = (Vector2) o;
        return x == vector2.x &&
                y == vector2.y;
    }

    @Override
    public int hashCode() {
        return Objects.hash(x, y);
    }

    @Override
    public String toString() {
        return "Vector2{" +
                "x=" + x +
                ", y=" + y +
                '}';
    }
}

package test.services;

import main.entities.GameSettings;
import main.entities.GameState;
import main.entities.Player;
import main.enums.CardStatus;
import main.enums.CardType;
import main.enums.GameStatus;
import main.enums.PlayerStatus;
import main.services.CardService;
import main.services.GameService;
import main.services.PlayerService;
import org.junit.Before;
import org.junit.Test;
import main.utility.Vector2;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class GameServiceTest {
    private GameService gameService;

    private PlayerService playerService;

    private CardService cardService;

    private GameState gameState;

    private GameSettings gameSettings;

    @Before
    public void setUp() {
        playerService = PlayerService.getInstance();
        gameService   = GameService  .getInstance();
        cardService   = CardService  .getInstance();

        playerService.reset();
        gameService  .reset();

        gameState    = gameService.getGameState();
        gameSettings = gameService.getGameSettings();
    }

    /**
     * Given: a move to a nearby spot
     * When: isMoveAllowed is called
     * Expect: the move is allowed
     */
    @Test
    public void isMoveAllowedNormal() {
        assertTrue(isMoveAllowed(
                new Vector2(0, 0),
                new Vector2(1, 0),
                new Vector2(0, 1),
                CardType.NONE
        ));
    }

    /**
     * Given: a move to a nearby spot that is occupied
     * When: isMoveAllowed is called
     * Expect: the move is not allowed
     */
    @Test
    public void isMoveAllowedNormalAlreadyOccupied() {
        assertFalse(isMoveAllowed(
                new Vector2(0, 0),
                new Vector2(1, 0),
                new Vector2(1, 0),
                CardType.NONE
        ));
    }

    /**
     * Given: a move to a distant spot
     * When: isMoveAllowed is called
     * Expect: the move is not allowed
     */
    @Test
    public void isMoveAllowedNormalTooFar() {
        assertFalse(isMoveAllowed(
                new Vector2(0, 0),
                new Vector2(1, 0),
                new Vector2(GameService.GAME_SIZE.getX() -1, 0),
                CardType.NONE
        ));
    }

    /**
     * Given: a move to a distant spot, with the ANYWHERE card used
     * When: isMoveAllowed is called
     * Expect: the move is allowed
     */
    @Test
    public void isMoveAllowedAnywhere() {
        assertTrue(isMoveAllowed(
                new Vector2(0, 0),
                new Vector2(1, 0),
                new Vector2(GameService.GAME_SIZE.getX() -1, GameService.GAME_SIZE.getY() - 1),
                CardType.ANYWHERE
        ));
    }

    /**
     * Given: a move to a nearby spot, which is occupied, with the REPLACE card used
     * When: isMoveAllowed is called
     * Expect: the move is allowed
     */
    @Test
    public void isMoveAllowedReplace() {
        assertTrue(isMoveAllowed(
                new Vector2(0, 0),
                new Vector2(1, 0),
                new Vector2(1, 0),
                CardType.REPLACE
        ));
    }

    /**
     * Given: player 1 makes a move to a free spot
     * When: makeMove is called
     * Expect: player 1 gains 1 point
     */
    @Test
    public void makeMoveFreeSpace() {
        makeMove(
                new Vector2(0, 0),
                new Vector2(1, 0),
                new Vector2(0, 1)
        );

        List<Player> players = new ArrayList<>(playerService.getPlayers());
        Player player1 = players.get(0);
        Player player2 = players.get(1);

        assertEquals(player1.getPoints(), 2);
        assertEquals(player2.getPoints(), 1);
    }

    /**
     * Given: player 1 makes a move to an occupied spot
     * When: makeMove is called
     * Expect: player 1 gains 1 point and player 2 loses 1 point
     */
    @Test
    public void makeMoveOccupiedSpace() {
        makeMove(
                new Vector2(0, 0),
                new Vector2(1, 0),
                new Vector2(1, 0)
        );

        List<Player> players = new ArrayList<>(playerService.getPlayers());
        Player player1 = players.get(0);
        Player player2 = players.get(1);

        assertEquals(player1.getPoints(), 2);
        assertEquals(player2.getPoints(), 0);
    }

    /**
     * Given: a filled board
     * When: the game is checked if it's over
     * Expect: the game status is set to over
     */
    @Test
    public void checkIfGameIsOver() {
        for (int row = 0; row < gameState.getColours().length; row++) {
            for (int col = 0; col < gameState.getColours()[0].length; col++) {
                gameState.getColours()[row][col] = 1;
            }
        }

        gameService.checkIfGameIsOver();

        assertEquals(gameState.getStatus(), GameStatus.OVER);
    }

    /**
     * Given: a player that is stuck and can no longer move
     * When: the game checks for lost players
     * Expect: the players' status is changed to lost
     */
    @Test
    public void checkIfPlayerLostWhenStuck() {
        createStuckPlayer(new Vector2(1, 1));

        List<Player> players = new ArrayList<>(playerService.getPlayers());
        Player player1 = players.get(0);

        for(int i = 0; i < player1.getCards().length; i++) {
            cardService.patchCard(CardStatus.USED.toString(),player1.getId(), i);
        }

        gameService.checkIfPlayersLost();

        assertEquals(player1.getStatus(), PlayerStatus.LOST);
    }

    /**
     * Given: a player that is already lost
     * When: the game checks for lost players
     * Expect: the game skips the player in calculations
     */
    @Test
    public void checkIfPlayerLostOptimization() {
        createStuckPlayer(new Vector2(1, 1));

        List<Player> players = new ArrayList<>(playerService.getPlayers());
        Player player1 = players.get(0);

        playerService.patchPlayer(PlayerStatus.LOST.toString(), player1.getId());

        for(int i = 0; i < player1.getCards().length; i++) {
            cardService.patchCard(CardStatus.USED.toString(),player1.getId(), i);
        }

        gameService.checkIfPlayersLost();

        assertEquals(player1.getStatus(), PlayerStatus.LOST);
    }

    /**
     * Given: a player that is stuck and can no longer move, but has the replace card still available
     * When: the game checks for lost players
     * Expect: the player hasn't lost yet
     */
    @Test
    public void checkIfPlayerLostWhenTheyStillHaveReplace() {
        createStuckPlayer(new Vector2(1, 1));

        List<Player> players = new ArrayList<>(playerService.getPlayers());
        Player player1 = players.get(0);
        PlayerStatus startingStatus = player1.getStatus();

        for(int i = 0; i < player1.getCards().length; i++) {
            if(player1.getCards()[i].getType() != CardType.REPLACE) {
                cardService.patchCard(CardStatus.USED.toString(), player1.getId(), i);
            }
        }

        gameService.checkIfPlayersLost();

        assertEquals(player1.getStatus(), startingStatus);
    }

    /**
     * Given: a player that is stuck and can no longer move, but has the anywhere card still available
     * When: the game checks for lost players
     * Expect: the player hasn't lost yet
     */
    @Test
    public void checkIfPlayerLostWhenTheyStillHaveAnywhere() {
        createStuckPlayer(new Vector2(1, 1));

        List<Player> players = new ArrayList<>(playerService.getPlayers());
        Player player1 = players.get(0);
        PlayerStatus startingStatus = player1.getStatus();

        for(int i = 0; i < player1.getCards().length; i++) {
            if(player1.getCards()[i].getType() != CardType.ANYWHERE) {
                cardService.patchCard(CardStatus.USED.toString(), player1.getId(), i);
            }
        }

        gameService.checkIfPlayersLost();

        assertEquals(player1.getStatus(), startingStatus);
    }

    /**
     * Given: a game with full players
     * When: isGameFullWhenItIs is called
     * Expect: the game should be full
     */
    @Test
    public void isGameFullWhenItIs() {
        createPlayers(GameService.MAXIMUM_PLAYERS, PlayerStatus.READY);

        assertTrue(gameService.isGameFull());
    }

    /**
     * Given: a game with no players
     * When: isGameFullWhenItIs is called
     * Expect: the game should not be full
     */
    @Test
    public void isGameFullnt() {
        createPlayers(0, PlayerStatus.READY);

        assertFalse(gameService.isGameFull());
    }

    /**
     * Given: a game with 2 players that are ready to play
     * When: the game checks if it should start
     * Expect: the game status is changed to STARTED
     */
    @Test
    public void shouldGameStartWithTwoPeople() {
        createPlayers(2, PlayerStatus.READY);

        assertEquals(gameState.getStatus(), GameStatus.STARTED);
    }

    /**
     * Given: a game with 1 players that is ready to play
     * When: the game checks if it should start
     * Expect: the game should not start, since it requires atleast 2 ready players
     */
    @Test
    public void shouldGameStartWithOnePerson() {
        createPlayers(1, PlayerStatus.READY);

        assertEquals(gameState.getStatus(), GameStatus.WAITING);
    }

    /**
     * Given: a game with 2 players that are playing
     * When: the game checks for the next player
     * Expect: the next player is returned
     */
    @Test
    public void nextPlayerWithPlayingPeople() {
        createPlayers(2, PlayerStatus.PLAYING);
        List<Player> players = new ArrayList<>(playerService.getPlayers());
        gameService.nextPlayer();
        assertEquals(gameState.getCurrentPlayer(), players.get(1).getId());
    }

    /**
     * Given: a game with 2 players, one that is playing and one that already lost
     * When: the game checks for the next player
     * Expect: the same player is returned
     */
    @Test
    public void nextPlayerWithALostPlayer() {
        createPlayers(2, PlayerStatus.PLAYING);
        List<Player> players = new ArrayList<>(playerService.getPlayers());
        playerService.patchPlayer(PlayerStatus.LOST.toString(), players.get(1).getId());
        gameService.nextPlayer();
        assertEquals(gameState.getCurrentPlayer(), players.get(0).getId());
    }

    /**
     * Create two players, one that starts in <playerPos>. The other player will occupy all spots around the first player.
     * @param playerPos : position of the first player
     */
    private void createStuckPlayer(final Vector2 playerPos) {
        createPlayers(2, PlayerStatus.NOTREADY);
        List<Player> players = new ArrayList<>(playerService.getPlayers());
        Player player1 = players.get(0);
        Player player2 = players.get(1);

        gameState.getColours()[playerPos.getX()][playerPos.getY()] = player1.getId();

        for(Vector2 neighbour : GameService.NEIGHBOUR_COORDS) {
            Vector2 position = playerPos.add(neighbour);
            if(position.isPositionValid(GameService.GAME_SIZE)) {
                gameState.getColours()[position.getX()][position.getY()] = player2.getId();
            }
        }
    }

    /**
     * Execute a move based on the given coordinates
     * @param player1Pos : creates a cell at this point for player 1
     * @param player2Pos : creates a cell at this point for player 2
     * @param moveAt : where the move will take place at
     */
    private void makeMove(final Vector2 player1Pos, final Vector2 player2Pos, final Vector2 moveAt) {
        createPlayers(2, PlayerStatus.READY);
        List<Player> players = new ArrayList<>(playerService.getPlayers());
        Player player1 = players.get(0);
        Player player2 = players.get(1);

        gameState.getColours()[player1Pos.getX()][player1Pos.getY()] = player1.getId();
        gameState.getColours()[player2Pos.getX()][player2Pos.getY()] = player2.getId();

        gameService.makeMove(moveAt.getX(), moveAt.getY(), player1.getId());
    }

    /**
     * Checks if a move is allowed to a given destination
     * @param player1Pos : creates a cell at this point for player 1
     * @param player2Pos : creates a cell at this point for player 1
     * @param moveAt : where the move will take place at
     * @param type : the card type that would be used by the move
     * @return
     */
    private boolean isMoveAllowed(final Vector2 player1Pos, final Vector2 player2Pos, final Vector2 moveAt, final CardType type) {
        createPlayers(2, PlayerStatus.READY);
        List<Player> players = new ArrayList<>(playerService.getPlayers());
        Player player1 = players.get(0);
        Player player2 = players.get(1);

        gameState.getColours()[player1Pos.getX()][player1Pos.getY()] = player1.getId();
        gameState.getColours()[player2Pos.getX()][player2Pos.getY()] = player2.getId();

        boolean answer = gameService.isMoveAllowed(moveAt.getX(), moveAt.getY(), player1.getId(), type);

        return answer;
    }

    /**
     * Creates a number of players and sets their status.
     * @param numberOfPlayers
     * @param status all players will be set to this
     */
    private void createPlayers(final int numberOfPlayers, final PlayerStatus status) {
        for(int i = 0; i < numberOfPlayers; i++) {
            playerService.addNewPlayer("Test Player" + (i + 1));
        }

        for(int i = 0; i < numberOfPlayers; i++) {
            playerService.patchPlayer(status.toString(), i + 1);
        }
    }
}

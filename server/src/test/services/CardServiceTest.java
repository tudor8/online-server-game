package test.services;

import main.entities.Card;
import main.entities.Player;
import main.enums.CardStatus;
import main.enums.CardType;
import org.junit.Before;
import org.junit.Test;
import main.services.CardService;
import main.services.PlayerService;

import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertFalse;

public class CardServiceTest {
    private PlayerService playerService;
    private CardService cardService;

    private Player player;
    private Card card;

    @Before
    public void setUp() {
        playerService = PlayerService.getInstance();
        cardService = CardService.getInstance();
        player = playerService.addNewPlayer("Test Player");
        card = player.getCards()[0];
    }

    /**
     * Given: a card
     * When: the status of the card is patched
     * Expect: it is properly set to that status
     */
    @Test
    public void patchCardWithGoodArguments() {
        boolean result = patchCard(CardStatus.SELECTED.toString(), CardStatus.SELECTED);

        assertTrue(result);
    }

    /**
     * Given: a card
     * When: the status of the card is patched with a bad argument
     * Expect: it is not properly set to that status
     */
    @Test
    public void patchCardWithBadArguments() {
        boolean result = patchCard("Bad Status", card.getState());

        assertFalse(result);
    }

    /**
     * Given: a card
     * When: the card is selected and then used
     * Expect: the card isn't the players' selected card
     */
    @Test
    public void useCard() {
        patchCard(CardStatus.SELECTED.toString(), CardStatus.SELECTED);
        assertTrue(player.getSelectedCard() == 0);
        patchCard(CardStatus.USED.toString(), CardStatus.USED);
        assertTrue(player.getSelectedCard() == -1);
    }

    /**
     * Given: a player with cards
     * When: a specific card is retrieved
     * Expect: the right card is found
     */
    @Test
    public void getCardWithProperArgument() {
        Card actual = cardService.getCardByType(CardType.ANYWHERE, player.getId());

        boolean foundCard = false;
        for(Card card : player.getCards()) {
            if(card.equals(actual)) {
                foundCard = true;
            }
        }

        assertTrue(foundCard);
    }

    /**
     * Given: a player with cards
     * When: a non-existent card is retrieved
     * Expect: no card is found
     */
    @Test
    public void getCardWithBadArgument() {
        Card actual = cardService.getCardByType(CardType.NONE, player.getId());

        boolean foundCard = false;
        for(Card card : player.getCards()) {
            if(card.equals(actual)) {
                foundCard = true;
            }
        }

        assertFalse(foundCard);
    }

    /**
     * Quick method to patch a card and check that the right status was changed
     * @param statusToChange attempt to change to this
     * @param statusToCheck check that it has properly changed
     * @return result of patching
     */
    private boolean patchCard(final String statusToChange, final CardStatus statusToCheck) {
        boolean actual = cardService.patchCard(statusToChange, player.getId(), 0);

        assertEquals(card.getState(), statusToCheck);

        return actual;
    }
}

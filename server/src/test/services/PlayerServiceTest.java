package test.services;

import main.entities.Player;
import main.enums.PlayerStatus;
import main.services.PlayerService;
import org.junit.Before;
import org.junit.Test;

import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertFalse;

public class PlayerServiceTest {
    private PlayerService playerService;

    private Player player;

    @Before
    public void setUp() {
        playerService = PlayerService.getInstance();
        player = playerService.addNewPlayer("Test Player");
    }

    /**
     * Given: a player
     * When: the status of the player is patched
     * Expect: it is properly set to that status
     */
    @Test
    public void patchPlayerWithGoodArguments() {
        boolean result = patchPlayer(PlayerStatus.PLAYING.toString(), PlayerStatus.PLAYING);

        assertTrue(result);
    }

    /**
     * Given: a player
     * When: the status of the player is patched with a bad status
     * Expect: it is not properly set to that status
     */
    @Test
    public void patchPlayerWithBadArguments() {
        boolean result = patchPlayer("Bad status", player.getStatus());

        assertFalse(result);
    }

    /**
     * Quick method to patch a player and check that the right status was changed
     * @param statusToChange attempt to change to this
     * @param statusToCheck check that it has properly changed
     * @return result of pathing
     */
    private boolean patchPlayer(final String statusToChange, final PlayerStatus statusToCheck) {
        boolean actual = playerService.patchPlayer(statusToChange, player.getId());

        assertEquals(player.getStatus(), statusToCheck);

        return actual;
    }

}

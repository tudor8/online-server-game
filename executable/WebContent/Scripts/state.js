var playerId;

var gameStateGlobal;
var playerListGlobal;
var gameSettingsGlobal;

var isFullGlobal = false;

function frameLoop() {
    executeGetRequest("game/state", [],
        function (gameState) {
            gameStateGlobal = gameState;
            updateGameGrid();

            executeGetRequest("players/getAll", [],
                function(playerList) {
                    if(!playerList || playerList.length == 0) {
                        $(location).attr('href', 'index.html');
                    }

                    playerListGlobal = playerList;

                    updatePlayers(playerList);
                    updatePlayerCards(playerList);
                    updateInfoBox();
                }
            );
        }
    );

    window.setTimeout(frameLoop.bind(null), 100);
}

function gameFullLoop() {
    executeGetRequest("game/isGameFull", [],
        function (isFull) {
            if(isFull) {
                continueText.style.visibility = "visible";
                isFullGlobal = true;
            }
            else {
                continueText.style.visibility = "hidden";
                isFullGlobal = false;
            }
        }
    );
    window.setTimeout(gameFullLoop.bind(null), 100);
}
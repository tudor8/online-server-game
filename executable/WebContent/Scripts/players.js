var playerNames;

function updatePlayers(newPlayers) {
    if(playerNames == null || playerNames.length != newPlayers.length) {
        createPlayers(newPlayers);
    }
    else {
        for (var index = 0; index < playerListGlobal.length; index++) {
            playerNames[index].text(playerListGlobal[index].name);
        }
    }
}

function createPlayers(newPlayers) {
    playerNames = new Array(playerListGlobal.length);

    var container = document.getElementById('playerList');

    container.innerHTML = "";

    for(var index = 0; index < newPlayers.length; index++) {
        var playerName = $('<p />')
            .attr('class', 'playerBoxElement')
            .attr('style', "color: #" + gameSettingsGlobal.colours[newPlayers[index].color])
            .text(newPlayers[index].name)
            .appendTo(container);

         playerNames[index] = playerName;
    }
}
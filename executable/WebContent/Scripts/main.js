function executeGetRequest(url, params, callback) {
    $.get(
        "rest/" + url,
        params,
        function(data) {
            callback(data);
        }
    );
}

function executePostRequest(url, params, callback) {
    $.post(
        "rest/" + url,
        params,
        function(data) {
            callback(data);
        }
    );
}
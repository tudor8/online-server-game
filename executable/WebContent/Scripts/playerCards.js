var playerTitles;
var playerCards;

var clickedOnCard = false;

function onReadyButtonClick(button) {
    executePostRequest(
        "players/patchPlayer",
        {"status":"ready", "playerId":playerId},
        function() {
            button.attr('style', "visibility: hidden");
        }
    );
}

function onCardClick(cardId) {
    var currentCard = playerListGlobal[playerId - 1].selectedCard;

    var isCardUsedAlready = currentCard == -1 ? false : playerListGlobal[playerId - 1].cards[currentCard].status == "USED";

    if( playerId != gameStateGlobal.currentPlayer ||
        gameStateGlobal.status.toUpperCase() != "STARTED" ||
        isCardUsedAlready
    ) {
        return;
    }

    if(clickedOnCard == true)
        return;

    clickedOnCard = true;

    if(currentCard != -1) {
        executePostRequest(
            "cards/patchCard",
            {"status":"AVAILABLE", "playerId":playerId, "cardId": currentCard},
            function() {
                if(cardId != currentCard) {
                    executePostRequest(
                        "cards/patchCard",
                        {"status":"SELECTED", "playerId":playerId, "cardId": cardId},
                        function() {
                            clickedOnCard = false;
                        }
                    );
                }
            }
        );
    }
    else {
        executePostRequest(
            "cards/patchCard",
            {"status":"SELECTED", "playerId":playerId, "cardId": cardId},
            function() {
                clickedOnCard = false;
            }
        );
    }


}

function getSelectedCardType() {
    switch(playerListGlobal[playerId - 1].selectedCard) {
    case -1:
        return "NONE";
    case 0:
        return "MULTIPLACE";
    case 1:
        return "REPLACE"
    case 2:
        return "ANYWHERE"
    }
}

function updatePlayerCards(newPlayers) {
    if(playerTitles == null || playerTitles.length != newPlayers.length) {
        createPlayerCards(newPlayers);
    }

    for (var index = 0; index < playerListGlobal.length; index++) {
        var displayStatus = playerListGlobal[index].status;
        if(displayStatus.toUpperCase() == "PLAYING")
            displayStatus = "";

        if(gameStateGlobal.status.toUpperCase() != "WAITING") {
            var pointText = playerListGlobal[index].points == 1 ? " point" : " points";
            if(displayStatus.length != 0) {
                displayStatus += " ";
            }
            displayStatus = displayStatus + playerListGlobal[index].points + pointText;
        }

        if((index + 1) == playerId) {
            playerTitles[index].text("You" + " (" + displayStatus + ")");
        }
        else {
            playerTitles[index].text(playerListGlobal[index].name + " (" + displayStatus + ")");
        }

        var cards = playerListGlobal[index].cards;
        for(var cardIndex = 0; cardIndex < 3; cardIndex++) {
            switch(cards[cardIndex].status.toUpperCase()) {
            case "USED":
                playerCards[index][cardIndex].attr('class', "card cardUsed");
                break;
            case "AVAILABLE":
                playerCards[index][cardIndex].attr('class', "card");
                break;
            case "SELECTED":
                playerCards[index][cardIndex].attr('class', "card cardSelected");
                break;
            }
        }
    }
}

function createPlayerCards(newPlayers) {
    playerTitles = new Array(playerListGlobal.length);
    playerCards = new Array(playerListGlobal.length);
    for (var i = 0; i < playerListGlobal.length; i++) {
        playerCards[i] = new Array(3);
    }

    var container = document.getElementById('optionBox');

    container.innerHTML = "";

    for (var index = 0; index < newPlayers.length; index++) {
        console.log(index);
        var playerOption = $('<div />').attr('class', 'playerOption');
        var titleWrapper = $('<div />').attr('class', 'titleWrapper').appendTo(playerOption);

        var playerText = $('<h1 />')
            .attr('class', 'title')
            .attr('style', "color: #" + gameSettingsGlobal.colours[newPlayers[index].color])
            .text(newPlayers[index].name + "(" + newPlayers[index].status + ")")
            .appendTo(titleWrapper);
        playerTitles[index] = playerText;

        var cardHolder =  $('<div />').attr('class', 'cardBox');
        var cards = newPlayers[index].cards;
        var degrees = -20;
        for(var cardIndex = 0; cardIndex < cards.length; cardIndex++) {
            (function(cardIndex, playerIndex) {
                var text = $('<p />').text(cards[cardIndex].type);
                var card = $('<div />')
                    .attr('class', 'card')
                    .attr('style', "transform: rotate(" + degrees + "deg)")
                    .append(text)
                    .appendTo(cardHolder);

                playerCards[playerIndex][cardIndex] = card;

                if(playerIndex == playerId - 1) {
                    card.click(function() { onCardClick(cardIndex)});
                }
            })(cardIndex, index);

            degrees += 20;
        }

        cardHolder.appendTo(playerOption);

        var readyButton = $('<button />')
            .attr('class', 'titleWrapper')
            .attr('style', "visibility:hidden")
            .text("I'm Ready!")
            .appendTo(playerOption);

        (function(readyButton) {
            if((index + 1) == playerId && gameStateGlobal.status.toUpperCase() != "STARTED") {
                readyButton.attr('style', "visibility:visible");
            }
            readyButton.click(function() { onReadyButtonClick(readyButton)});
        })(readyButton);

        playerOption.appendTo(container);
    }
}
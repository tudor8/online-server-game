var gridBoxes;

var clicked = false;
var movesDoneThisTurn = 0;

function onGridBoxClick(row, col) {
    if(playerId != gameStateGlobal.currentPlayer || gameStateGlobal.status.toUpperCase() != "STARTED")
        return;

    if(clicked == true)
        return;

    clicked = true;
    executeGetRequest("game/isMoveAllowed",
        {"x":row, "y":col, "playerId": playerId, "cardType": getSelectedCardType()},
        function (isAllowed) {
            if(isAllowed) {
                executePostRequest(
                    "game/makeMove",
                    {"x":row, "y":col, "playerId":playerId},
                    function() {
                        movesDoneThisTurn++;
                        var canMoveAgain = false;
                        if(getSelectedCardType() == "MULTIPLACE") {
                            if(movesDoneThisTurn == 1) {
                                canMoveAgain = true;
                            }
                            else {movesDoneThisTurn = 0; }
                        }
                        else { movesDoneThisTurn = 0; }

                        if(!canMoveAgain) {
                            executePostRequest("game/nextPlayer",{}, function() {clicked = false;} );
                        }

                        if(playerListGlobal[playerId - 1].selectedCard != -1) {
                            executePostRequest(
                                "cards/patchCard",
                                {"status":"USED", "playerId":playerId, "cardId": playerListGlobal[playerId - 1].selectedCard},
                                function() {clicked = false;}
                            );
                        }
                    }
                );
            }
            else {
                clicked = false;
            }
        }
    );
}

function updateGameGrid() {
    if(gridBoxes == null) {
        createGameGrid();
    }
    else {
        var gridColours = gameStateGlobal.colours;
        for (row = 0; row < gameSettingsGlobal.x; row++) {
            for(col = 0; col < gameSettingsGlobal.y; col++) {
                var colour = gameSettingsGlobal.colours[gridColours[row][col]];
                gridBoxes[row][col].attr('style',
                    'width: '  + (100 / gameSettingsGlobal.y) + "%;" +
                    'height: ' + (100 / gameSettingsGlobal.x) + "%;" +
                    'background-color: '  + colour + ";"
                )
            }
        }
    }
}

function createGameGrid() {
    gridBoxes = new Array(gameSettingsGlobal.x);
    for (var i = 0; i < gameSettingsGlobal.x; i++) {
        gridBoxes[i] = new Array(gameSettingsGlobal.y);
    }

    var container = document.getElementById('gridBox');

    var max = Math.max(gameSettingsGlobal.x, gameSettingsGlobal.y);

    var gridColours = gameStateGlobal.colours;
    for (row = 0; row < gameSettingsGlobal.x; row++) {
        var newRow = $('<tr />');
        for(col = 0; col < gameSettingsGlobal.y; col++) {
            (function(row, col) {
                var colour = gameSettingsGlobal.colours[gridColours[row][col]];
                var newDiv = $('<td /> ')
                    .attr('style',
                        'width: '  + (100 / gameSettingsGlobal.y) + "%;" +
                        'height: ' + (100 / gameSettingsGlobal.x) + "%;" +
                        'background-color: '  + colour + ";"
                    )
                    .click(function() { onGridBoxClick(row, col);})
                    .attr('class', 'gridBoxElement')
                    .appendTo(newRow);

                gridBoxes[row][col] = newDiv;
            })(row, col);
        }
        newRow.appendTo(container);
    }
}
function updateInfoBox() {
    var container = document.getElementById('timerBox').getElementsByClassName("title")[0];

    switch(gameStateGlobal.status.toUpperCase()) {
    case "WAITING":
        container.innerHTML = "Waiting for players...";
        break;
    case "STARTED":
        container.innerHTML = "Current Player: " + playerListGlobal[gameStateGlobal.currentPlayer - 1].name;
        break;
    case "OVER":
        var max = 0;
        var winnerIndexes = -1;
        for (var index = 0; index < playerListGlobal.length; index++) {
            // If two people have the same score, the last joined wins (also the last in the player list global)
            if(playerListGlobal[index].points >= max) {
                max = playerListGlobal[index].points;
                winnerIndex = playerListGlobal[index].id;
            }
        }
        var name = playerListGlobal[winnerIndex - 1].name;
        if(winnerIndex == playerId - 1) {
            name = "You";
        }
        container.innerHTML = "The winner was: " + name + "!";
        break;
    default:
        container.innerHTML = "Invalid Game State";
        break;
    }
}
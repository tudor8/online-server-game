function validateForm() {
    var nameText  = document.forms["nameForm"]["nameInput"];

    var errorText = document.getElementById('errorText');
    var continueText = document.getElementById('continueText');

    if (nameText.value == "") {
        errorText.innerHTML = "Please input a name in the box above."
    }
    else {
        errorText.innerHTML = "";
    }

    if(!isFullGlobal) {
        executePostRequest(
            "players/newPlayer",
            {"name": nameText.value},
            function(player) {
                console.log(player.id);
                playerId = player.id;
                sessionStorage.setItem("playerId", playerId);
                $(location).attr('href', 'game.html')
            }
        );
    }

    return false;
}